package com.example.android.snookercounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String STATE_RED_BALLS = "red_balls";
    private static final String STATE_YELLOW_BALLS = "yellow_balls";
    private static final String STATE_GREEN_BALLS = "green_balls";
    private static final String STATE_BROWN_BALLS = "brown_balls";
    private static final String STATE_BLUE_BALLS = "blue_balls";
    private static final String STATE_PINK_BALLS = "pink_balls";
    private static final String STATE_BLACK_BALLS = "black_balls";
    private static final String STATE_REMAINING_POINTS = "remaining_points";
    private static final String STATE_PLAYER_A_POINTS = "player_a_points";
    private static final String STATE_PLAYER_B_POINTS = "player_b_points";

    private int redBalls = 15;
    private int yellowBalls = 1;
    private int greenBalls = 1;
    private int brownBalls = 1;
    private int blueBalls = 1;
    private int pinkBalls = 1;
    private int blackBalls = 1;
    private int remainingPoints = 0;
    private int playerAPoints = 0;
    private int playerBPoints = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // If we have a saved state then we can restore it now
        if (savedInstanceState != null) {
            this.redBalls = savedInstanceState.getInt(STATE_RED_BALLS, 15);
            this.yellowBalls = savedInstanceState.getInt(STATE_YELLOW_BALLS, 1);
            this.greenBalls = savedInstanceState.getInt(STATE_GREEN_BALLS, 1);
            this.brownBalls = savedInstanceState.getInt(STATE_BROWN_BALLS, 1);
            this.blueBalls = savedInstanceState.getInt(STATE_BLUE_BALLS, 1);
            this.pinkBalls = savedInstanceState.getInt(STATE_PINK_BALLS, 1);
            this.blackBalls = savedInstanceState.getInt(STATE_BLACK_BALLS, 1);
            this.remainingPoints = savedInstanceState.getInt(STATE_REMAINING_POINTS, 0);
            this.playerAPoints = savedInstanceState.getInt(STATE_PLAYER_A_POINTS, 0);
            this.playerBPoints = savedInstanceState.getInt(STATE_PLAYER_B_POINTS, 0);
        }

        displayForPlayerA();
        displayForPlayerB();
        displayRemainingPoints();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Make sure to call the super method so that the states of our views are saved
        super.onSaveInstanceState(outState);
        // Save our own state now
        outState.putSerializable(STATE_RED_BALLS, this.redBalls);
        outState.putSerializable(STATE_YELLOW_BALLS, this.yellowBalls);
        outState.putSerializable(STATE_GREEN_BALLS, this.greenBalls);
        outState.putSerializable(STATE_BROWN_BALLS, this.brownBalls);
        outState.putSerializable(STATE_BLUE_BALLS, this.blueBalls);
        outState.putSerializable(STATE_PINK_BALLS, this.pinkBalls);
        outState.putSerializable(STATE_BLACK_BALLS, this.blackBalls);
        outState.putSerializable(STATE_REMAINING_POINTS, this.remainingPoints);
        outState.putSerializable(STATE_PLAYER_A_POINTS, this.playerAPoints);
        outState.putSerializable(STATE_PLAYER_B_POINTS, this.playerBPoints);
    }

    /**
     * Reset scores and number of balls.
     *
     * @param view
     */
    public void resetScores(View view) {
        redBalls = 15;
        yellowBalls = 1;
        greenBalls = 1;
        brownBalls = 1;
        blueBalls = 1;
        pinkBalls = 1;
        blackBalls = 1;
        playerAPoints = 0;
        playerBPoints = 0;

        displayForPlayerA();
        displayForPlayerB();
        displayRemainingPoints();
    }

    /**
     * Adds 1 point to player A.
     *
     * @param view
     */
    public void playerA1Point(View view) {
        if (this.redBalls > 0) {
            this.redBalls--;
            this.playerAPoints++;
            displayForPlayerA();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 1 point to player B.
     *
     * @param view
     */
    public void playerB1Point(View view) {
        if (this.redBalls > 0) {
            this.redBalls--;
            this.playerBPoints++;
            displayForPlayerB();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 2 points to player A.
     *
     * @param view
     */
    public void playerA2Points(View view) {
        if (this.yellowBalls > 0) {
            this.playerAPoints += 2;
            if (this.redBalls <= 0) {
                this.yellowBalls--;
            }
            displayForPlayerA();
            displayRemainingPoints();
        }

    }

    /**
     * Adds 2 points to player B.
     *
     * @param view
     */
    public void playerB2Points(View view) {
        if (this.yellowBalls > 0) {
            this.playerBPoints += 2;
            if (this.redBalls <= 0) {
                this.yellowBalls--;
            }
            displayForPlayerB();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 3 points to player A.
     *
     * @param view
     */
    public void playerA3Points(View view) {
        if (this.greenBalls > 0) {
            this.playerAPoints += 3;
            if (this.redBalls <= 0) {
                this.greenBalls--;
            }
            displayForPlayerA();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 3 points to player B.
     *
     * @param view
     */
    public void playerB3Points(View view) {
        if (this.greenBalls > 0) {
            this.playerBPoints += 3;
            if (this.redBalls <= 0) {
                this.greenBalls--;
            }
            displayForPlayerB();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 4 points to player A.
     *
     * @param view
     */
    public void playerA4Points(View view) {
        if (this.brownBalls > 0) {
            this.playerAPoints += 4;
            if (this.redBalls <= 0) {
                this.brownBalls--;
            }
            displayForPlayerA();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 4 points to player B.
     *
     * @param view
     */
    public void playerB4Points(View view) {
        if (this.brownBalls > 0) {
            this.playerBPoints += 4;
            if (this.redBalls <= 0) {
                this.brownBalls--;
            }
            displayForPlayerB();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 5 points to player A.
     *
     * @param view
     */
    public void playerA5Points(View view) {
        if (this.blueBalls > 0) {
            this.playerAPoints += 5;
            if (this.redBalls <= 0) {
                this.blueBalls--;
            }
            displayForPlayerA();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 5 points to player B.
     *
     * @param view
     */
    public void playerB5Points(View view) {
        if (this.blueBalls > 0) {
            this.playerBPoints += 5;
            if (this.redBalls <= 0) {
                this.blueBalls--;
            }
            displayForPlayerB();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 6 points to player A.
     *
     * @param view
     */
    public void playerA6Points(View view) {
        if (this.pinkBalls > 0) {
            this.playerAPoints += 6;
            if (this.redBalls <= 0) {
                this.pinkBalls--;
            }
            displayForPlayerA();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 6 points to player B.
     *
     * @param view
     */
    public void playerB6Points(View view) {
        if (this.pinkBalls > 0) {
            this.playerBPoints += 6;
            if (this.redBalls <= 0) {
                this.pinkBalls--;
            }
            displayForPlayerB();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 7 points to player A.
     *
     * @param view
     */
    public void playerA7Points(View view) {
        if (this.blackBalls > 0) {
            this.playerAPoints += 7;
            if (this.redBalls <= 0) {
                this.blackBalls--;
            }
            displayForPlayerA();
            displayRemainingPoints();
        }
    }

    /**
     * Adds 7 points to player B.
     *
     * @param view
     */
    public void playerB7Points(View view) {
        if (this.blackBalls > 0) {
            this.playerBPoints += 7;
            if (this.redBalls <= 0) {
                this.blackBalls--;
            }
            displayForPlayerB();
            displayRemainingPoints();
        }
    }

    /**
     * Display score for player A.
     */
    public void displayForPlayerA() {
        TextView scoreView = (TextView) findViewById(R.id.text_player_a_score);
        scoreView.setText(String.valueOf(this.playerAPoints));
    }

    /**
     * Display score for player B.
     */
    public void displayForPlayerB() {
        TextView scoreView = (TextView) findViewById(R.id.text_player_b_score);
        scoreView.setText(String.valueOf(this.playerBPoints));
    }

    /**
     * Display remaining points.
     */
    public void displayRemainingPoints() {
        this.remainingPoints = (8 * this.redBalls) +
                (2 * this.yellowBalls) +
                (3 * this.greenBalls) +
                (4 * this.brownBalls) +
                (5 * this.blueBalls) +
                (6 * this.pinkBalls) +
                (7 * this.blackBalls);
        TextView scoreView = (TextView) findViewById(R.id.text_remaining_points);
        scoreView.setText(String.valueOf(this.remainingPoints));
    }
}
